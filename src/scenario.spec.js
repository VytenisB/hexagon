const {
    strategy,
    transferFromTo,
    getBasicCellInfo,
    makeCellsArrayToMap,
    getCellWithMinProp,
    getNeighborByType,
    getAllNeighborByType,
    hasEnoughResourceToTakeEnemy
} = require("./index.ts");

/* eslint-disable */

const SCENARIO_START = [
    { id: "a", neighbours: [{ id: "b", owner: 2, resources: 1 }], resources: 10 }
];

const SCENARIO_AFTER_START = [{
        resources: 2,
        id: "6|0",
        neighbours: [
            { owner: 0, resources: 3, id: "6|1" },
            { owner: 0, resources: 4, id: "5|0" },
            { owner: 0, resources: 2, id: "7|0" }
        ]
    },
    {
        resources: 3,
        id: "6|1",
        neighbours: [
            { owner: 0, resources: 2, id: "6|0" },
            { owner: 2, resources: 0, id: "6|2" },
            { owner: 0, resources: 4, id: "5|0" },
            { owner: 0, resources: 2, id: "7|0" },
            { owner: 2, resources: 0, id: "5|1" },
            { owner: 2, resources: 0, id: "7|1" }
        ]
    },
    {
        resources: 4,
        id: "5|0",
        neighbours: [
            { owner: 0, resources: 2, id: "6|0" },
            { owner: 0, resources: 3, id: "6|1" },
            { owner: 2, resources: 0, id: "5|1" },
            { owner: 0, resources: 5, id: "4|1" }
        ]
    },
    {
        resources: 2,
        id: "7|0",
        neighbours: [
            { owner: 0, resources: 2, id: "6|0" },
            { owner: 0, resources: 3, id: "6|1" },
            { owner: 2, resources: 0, id: "7|1" },
            { owner: 2, resources: 0, id: "8|1" }
        ]
    },
    {
        resources: 5,
        id: "4|1",
        neighbours: [
            { owner: 0, resources: 4, id: "5|0" },
            { owner: 2, resources: 0, id: "5|1" },
            { owner: 2, resources: 0, id: "4|2" },
            { owner: 0, resources: 6, id: "3|1" }
        ]
    },
    {
        resources: 6,
        id: "3|1",
        neighbours: [
            { owner: 0, resources: 5, id: "4|1" },
            { owner: 2, resources: 0, id: "4|2" },
            { owner: 2, resources: 0, id: "3|2" },
            { owner: 0, resources: 7, id: "2|2" }
        ]
    },
    {
        resources: 7,
        id: "2|2",
        neighbours: [
            { owner: 0, resources: 6, id: "3|1" },
            { owner: 2, resources: 0, id: "3|2" },
            { owner: 0, resources: 8, id: "2|3" },
            { owner: 2, resources: 0, id: "1|2" }
        ]
    },
    {
        resources: 8,
        id: "2|3",
        neighbours: [
            { owner: 0, resources: 7, id: "2|2" },
            { owner: 2, resources: 0, id: "3|2" },
            { owner: 2, resources: 0, id: "3|3" },
            { owner: 0, resources: 9, id: "2|4" },
            { owner: 2, resources: 0, id: "1|2" },
            { owner: 2, resources: 0, id: "1|3" }
        ]
    },
    {
        resources: 9,
        id: "2|4",
        neighbours: [
            { owner: 0, resources: 8, id: "2|3" },
            { owner: 2, resources: 0, id: "3|3" },
            { owner: 2, resources: 0, id: "3|4" },
            { owner: 0, resources: 10, id: "2|5" },
            { owner: 2, resources: 0, id: "1|3" },
            { owner: 2, resources: 0, id: "1|4" }
        ]
    },
    {
        resources: 10,
        id: "2|5",
        neighbours: [
            { owner: 0, resources: 9, id: "2|4" },
            { owner: 2, resources: 0, id: "3|4" },
            { owner: 2, resources: 0, id: "3|5" },
            { owner: 0, resources: 11, id: "2|6" },
            { owner: 2, resources: 0, id: "1|4" },
            { owner: 2, resources: 0, id: "1|5" }
        ]
    },
    {
        resources: 11,
        id: "2|6",
        neighbours: [
            { owner: 0, resources: 10, id: "2|5" },
            { owner: 2, resources: 0, id: "3|5" },
            { owner: 2, resources: 0, id: "3|6" },
            { owner: 0, resources: 12, id: "2|7" },
            { owner: 2, resources: 0, id: "1|5" },
            { owner: 2, resources: 0, id: "1|6" }
        ]
    },
    {
        resources: 12,
        id: "2|7",
        neighbours: [
            { owner: 0, resources: 11, id: "2|6" },
            { owner: 2, resources: 0, id: "3|6" },
            { owner: 2, resources: 0, id: "3|7" },
            { owner: 0, resources: 21, id: "2|8" },
            { owner: 2, resources: 0, id: "1|6" },
            { owner: 2, resources: 0, id: "1|7" }
        ]
    },
    {
        resources: 21,
        id: "2|8",
        neighbours: [
            { owner: 0, resources: 12, id: "2|7" },
            { owner: 2, resources: 0, id: "3|7" },
            { owner: 2, resources: 0, id: "3|8" },
            { owner: 2, resources: 0, id: "2|9" },
            { owner: 2, resources: 0, id: "1|7" },
            { owner: 2, resources: 0, id: "1|8" }
        ]
    }
];

describe("Strategy", () => {
    it("runs initial", () => {
        expect(strategy(SCENARIO_START)).toEqual(transferFromTo("a", "b", 1));
    });
});

describe("getBasicCellInfo", () => {
    test("works correctly in simple case", () => {
        const cell = {
            resources: 2,
            id: "6|0",
            neighbours: [
                { owner: 1, resources: 3, id: "6|1" },
                { owner: 2, resources: 4, id: "5|0" },
                { owner: 2, resources: 2, id: "7|0" }
            ]
        };

        expect(
            getBasicCellInfo(cell.id, cell, makeCellsArrayToMap(SCENARIO_AFTER_START))
        ).toEqual({
            id: cell.id,
            numberOfEnemies: 1,
            numberOfNone: 2,
            depthOfSafety: 0,
            strengthOfSafety: 0
        });
    });

    test("it works with more complex cases", () => {
        const cell = {
            resources: 4,
            id: "5|0",
            neighbours: [
                { owner: 0, resources: 2, id: "6|0" },
                { owner: 0, resources: 3, id: "6|1" },
                { owner: 2, resources: 0, id: "5|1" },
                { owner: 0, resources: 5, id: "4|1" }
            ]
        };

        expect(
            getBasicCellInfo(cell.id, cell, makeCellsArrayToMap(SCENARIO_AFTER_START))
        ).toEqual({
            id: cell.id,
            numberOfEnemies: 0,
            numberOfNone: 1,
            depthOfSafety: 0,
            strengthOfSafety: 10
        });
    });
});

describe('getCellWithMinProp', () => {
    it('correctly returns min value object', () => {
        const cells = [{ v: 0 }, { v: 1 }, { v: 3 }, { v: 2 }];

        expect(getCellWithMinProp(cells, 'v')).toEqual([{ v: 1 }, 1]);
    });

    it('correctly returns min value object 2', () => {
        const cells = [{ v: 2 }, { v: 2 }, { v: 3 }, { v: 2 }];

        expect(getCellWithMinProp(cells, 'v')).toEqual([{ v: 2 }, 2]);
    });

    it('correctly returns min value object 3', () => {
        const cells = [{ v: 0 }, { v: 0 }, { v: 0 }, { v: 0 }];

        expect(getCellWithMinProp(cells, 'v')).toEqual([{ v: 0 }, 0]);
    });
})

describe('getNeighborByType', () => {
    it('return all cells', () => {
        const testData = {
            neighbours: [{ owner: 1, a: 2 }, { owner: 1, a: 1 }, { owner: 0 }]
        }
        expect(getAllNeighborByType(testData, 1)).toEqual([{ owner: 1, a: 2 }, { owner: 1, a: 1 }]);
    });

    it('return 1 cells', () => {
        const testData = {
            neighbours: [{ owner: 1, a: 2 }, { owner: 1, a: 1 }, { owner: 0 }]
        }
        expect(getNeighborByType(testData, 1)).toEqual({ owner: 1, a: 2 });
    });

    it('return null', () => {
        const testData = {
            neighbours: [{ owner: 1, a: 2 }, { owner: 1, a: 1 }, { owner: 0 }]
        }
        expect(getNeighborByType(testData, 2)).toEqual(null);
    });

})


describe('hasEnoughResourceToTakeEnemy', () => {


    it('returns a weak enemy', () => {
        const cell = {
            resources: 4,
            id: "5|0",
            neighbours: [
                { owner: 1, resources: 2, id: "6|0" },
                { owner: 1, resources: 3, id: "6|1" },
                { owner: 0, resources: 1, id: "5|1" },
                { owner: 1, resources: 5, id: "4|1" }
            ]
        };

        expect(hasEnoughResourceToTakeEnemy(cell)).toEqual({ owner: 1, resources: 2, id: "6|0" });
    });

    it('returns an undefined if no enemy is weaker', () => {
        const cell = {
            resources: 4,
            id: "5|0",
            neighbours: [
                { owner: 1, resources: 5, id: "6|0" },
                { owner: 1, resources: 4, id: "6|1" },
                { owner: 0, resources: 1, id: "5|1" },
                { owner: 1, resources: 5, id: "4|1" }
            ]
        };

        expect(hasEnoughResourceToTakeEnemy(cell)).toEqual(undefined);
    })
})
