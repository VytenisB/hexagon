
interface Neighbor {
    id: string,
    resources: number,
    owner: number
}

interface Cell {
    id: string,
    neighbours: Array<Neighbor>,
    resources: number
}

interface CellBasicInfo {
    id: string,
    numberOfEnemies: number,
    numberOfNone: number,
    depthOfSafety: number,
    strengthOfSafety: number
}

const basicInfo = {
    numberOfEnemies: 'numberOfEnemies',
    numberOfNone: 'numberOfNone',
    depthOfSafety: 'depthOfSafety',
    strengthOfSafety: 'strengthOfSafety'
}

const OWNER_TYPES = {
    OWN: 0,
    ENEMY: 1,
    NONE: 2
};

const RESOURCE_SUPPLY_LIMIT = 100;

// cells that are most safe
// cells that need resources the most



export function strategy(boardState: Array<Cell>) {
    let allCellsInfo: Array<CellBasicInfo>;
    let weakerEnemy;
    let safest;
    let leastEnemy;
    let leastEnemyTWO;
    try {
        const stateMap = makeCellsArrayToMap(boardState);
        allCellsInfo = boardState.map(c => getBasicCellInfo(c.id, c, stateMap));


        // FREE CELL TAKING
        const [mostFreeNeighborsCell, freeCount] = getCellWithMinProp(allCellsInfo, basicInfo.numberOfNone);
        if (freeCount) return transferToFree(mostFreeNeighborsCell, stateMap);

        // ATTACK OR TRANSFER
        leastEnemy = getCellWithMinProp(allCellsInfo, basicInfo.numberOfEnemies);
        leastEnemyTWO = stateMap[leastEnemy.id];
        const [leastEnemyFacingCell, enemyCount] = getCellWithMinProp(allCellsInfo, basicInfo.numberOfEnemies);
        weakerEnemy = hasEnoughResourceToTakeEnemy(stateMap[leastEnemyFacingCell.id]);

        if (weakerEnemy) {
            // throw 'transferToEnemy' + JSON.stringify(transferToEnemy(stateMap[leastEnemyFacingCell.id], weakerEnemy));
           return transferToEnemy(stateMap[leastEnemyFacingCell.id], weakerEnemy);
        } else {
            safest = getSafestAndHealthiestCell(allCellsInfo, leastEnemyFacingCell.id, stateMap);
            // throw 'transferToOwn' + JSON.stringify(transferToOwn(leastEnemyFacingCell.id, getSafestAndHealthiestCell(allCellsInfo, leastEnemyFacingCell.id, stateMap)))
            return transferToOwn(leastEnemyFacingCell.id, getSafestAndHealthiestCell(allCellsInfo, leastEnemyFacingCell.id, stateMap));
        }

        // TODO
        // Improve healthiest selector: look at fnc for mostResourced
        // Improve weakest enemy selection
        // getSafestAndHealthiestCell: make so it can't take same as target


    } catch (error) {
        console.log({ allCellsInfo });
        log({
            error,
            leastEnemy,
            leastEnemyTWO,
            weakerEnemy,
            safest
        });
    }


    // ## main decision is to attack or to transfer
    // for resources transfer need to know which cells are safest and which need resources
    // for atack need to know which are strongest cells and which are weakest enemies

    // if empty cell is close by take it
    // if cell is surounded by own cell, transfer its resources to outer cells: towards enemy
    // prioratize enemy cells that are facin
    // keep shape close to circle
}



export function getBasicCellInfo(
    cellId: string,
    cell: Cell,
    stateMap: Object,
    ignoreIds = [],
    depth = 0,
    info = { id: '', numberOfEnemies: 0, numberOfNone: 0, depthOfSafety: 0, strengthOfSafety: 0 },
): CellBasicInfo {
    info.id = cellId;
    const ownNeighborIds = [];

    for (let neighboringCell of cell.neighbours) {

        if (ignoreIds.indexOf(neighboringCell.id) > -1) continue;

        if (depth === 0 && neighboringCell.owner === OWNER_TYPES.ENEMY) {
            info.numberOfEnemies++;
        } else if (depth === 0 && neighboringCell.owner === OWNER_TYPES.NONE) {
            info.numberOfNone++;
        } else if (neighboringCell.owner === OWNER_TYPES.OWN) {
            ownNeighborIds.push(neighboringCell.id);
        }
    }

    info.strengthOfSafety += ownNeighborIds.length;
    if (!info.numberOfEnemies && !info.numberOfNone) info.depthOfSafety++;

    if (depth === 2) return info;

    for (let neighborId of ownNeighborIds) {

        getBasicCellInfo(
            cellId,
            stateMap[neighborId],
            stateMap,
            ownNeighborIds.concat(ignoreIds),
            depth + 1,
            info
        );
    }

    return info;
}

export function makeCellsArrayToMap(cellsArray: Array<Cell>) {
    return cellsArray.reduce((o, cell) => {
        o[cell.id] = cell;
        return o;
    }, {});
}

export function transferFromTo(fromId, toId, amountToTransfer) {
    return { fromId, toId, amountToTransfer };
}

function transferToFree(mostFreeNeighborsCell, stateMap) {
    let r = transferFromTo(mostFreeNeighborsCell.id, getNeighborByType(stateMap[mostFreeNeighborsCell.id], OWNER_TYPES.NONE).id, 1);
    debugTransfer(r);
    return r;
}

function debugTransfer(r) {
    if (r.amountToTransfer === undefined || !r.fromId || !r.toId) {
        throw 'No vars' + r.amountToTransfer + ' ' + r.fromId + ' ' + r.toId;
    }
}


export function getNeighborByType(cell: Cell, type) {
    for (let n of cell.neighbours) {
        if (n.owner === type) {
            return n;
        }
    }

    return null;
}

export function getAllNeighborByType(cell: Cell, type) {
    const allNeighbors = [];
    for (let n of cell.neighbours) {
        if (n.owner === type) {
            allNeighbors.push(n);
        }
    }

    return allNeighbors;
}

export function hasEnoughResourceToTakeEnemy(cell: Cell) {
    const MIN_DIFF = 1;
    const enemies = getAllNeighborByType(cell, OWNER_TYPES.ENEMY);
    const weakerEnemies = [];

    for (let enemy of enemies) {
        if (enemy.resources < cell.resources - MIN_DIFF) {
            weakerEnemies.push(enemy);
        }
    }

    weakerEnemies.sort((a, b) => a.resources - b.resources);

    return weakerEnemies[0];
}

function transferToEnemy(cell: Cell, enemy: Neighbor) {
    return transferFromTo(cell.id, enemy.id, cell.resources - 1);
}


function transferToOwn(targetId: string, donor: Cell) {
    return transferFromTo(donor.id, targetId, donor.resources - 1);
}

function getSafestAndHealthiestCell(cellsInfo: Array<CellBasicInfo>, excludeId: string, cellMap): Cell {

    const mostResourced = cellsInfo.slice().sort((a, b) => {
        return cellMap[b.id].resources - cellMap[a.id].resources
    });

    const mostSafe = cellsInfo.slice().sort((a, b) => b.strengthOfSafety - a.strengthOfSafety);

    for (let cellInfo of mostResourced) {
        if (cellInfo.depthOfSafety >= 1) {
            return cellMap[cellInfo.id];
        }
    }

    return cellMap[mostSafe[0].id]; // TODO improve, try to use most resourced if it's say as safe as 30% of the safest, else look for most resouced in safest 30%
}

function getCellWithMaxProp(cells, prop) {
    return cells.reduce((winner, nextCell) => {
        return nextCell[prop] > winner[prop] ? nextCell : winner;
    })
}

/**
 * Return item that has minimum of value of given prop.
 * 
 * @export
 * @param {any} cells 
 * @param {any} prop 
 * @param {number} [min=1] 
 * @returns {[CellBasicInfo, number]} 
 */
export function getCellWithMinProp(cells, prop, min = 1): [CellBasicInfo, number] {
    const winner = cells.reduce((winner, nextCell) => {
        return (nextCell[prop] < winner[prop]) && nextCell[prop] >= min ? nextCell : winner[prop] >= min ? winner : nextCell;
    })

    return [winner, winner[prop]];
}

function log(m) {
    throw Error(JSON.stringify(m));
}
