const fs = require('fs');

const fileContent = fs.readFileSync('./dist/bundle.js', 'utf-8');

const appendPart = `
    function strategy(p) {
        return api.strategy(p);
    }
`;

fs.writeFileSync('./dist/bundle.js', fileContent + appendPart, 'utf-8');
