const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
    entry: './src/index.ts',
    resolve: {
        extensions: ['.js', '.ts']
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        library: 'api',
        libraryTarget: 'var'
    },
    target: 'web',
    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'awesome-typescript-loader' }
        ]
    },
    plugins: [
        new UglifyJSPlugin()
    ]
};

// resources
// https://www.npmjs.com/package/awesome-typescript-loader
// https://github.com/coryhouse/react-slingshot/blob/master/webpack.config.dev.js

// TODO
// - split into dev and prod configurations
